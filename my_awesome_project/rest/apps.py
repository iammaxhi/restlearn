from django.apps import AppConfig


class RestAppConfig(AppConfig):
    name = "rest"
    verbose_name = "Rest"
